#Tecnologias
Java
PostgreSQL
Lombok
Api Spring


#Instalação do Docker
https://medium.com/@renato.groffe/postgresql-docker-executando-uma-inst%C3%A2ncia-e-o-pgadmin-4-a-partir-de-containers-ad783e85b1a4

https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt

Instalação do Container com o Postgre
https://medium.com/@renato.groffe/postgresql-pgadmin-4-docker-compose-montando-rapidamente-um-ambiente-para-uso-55a2ab230b89


#Configuração e Execução do Docker
Salvar arquivo docker-compose.yml que esta versionado na pasta utils/docker.
Pasta onde se encontra o arquivo docker-compose.yml. Versionado na pasta utils/docker
Configurar arquivo docker-compose.yml de acordo onde será armazenado os arquivos do docker.
Subir docker: docker-compose up -d
Verificar se docker esta ativo: docker-compose ps

#Instalação Lombok
https://projectlombok.org/download
java -jar lombok.jar

#Instalação Maven
sudo apt install maven

#Execução do java#
mvn compile
mvn install - Gerar pacote da aplicação.

Copiar o arquivo application.properties para pasta target caso não tenha sido copiado
Obs: Após primeira executação, comentar a linha 13 do arquivo application.properties.
Esse linha é responsável por criar a tabela no docker quando a aplicação é iniciada.
 #spring.jpa.hibernate.ddl-auto=create
 
Pasta target:
#Executar o jar gerado. 
Pacote gerado com todas dependências.
java -jar api-client-0.0.1-SNAPSHOT.jar


#APIs

#Cliente

GET

http://{baseurl}/customers -> Retorna a lista de todos clientes

http://{baseurl}/customers/{id}-> Retorna o cliente pelo id

http://{baseurl}/customers?name={nome} -> Retorna lista de cliente pelo nome

http://{baseurl}/customers?email={email} -> Retorna lista de cliente pelo email

http://{baseurl}/customers?name={nome}&email={email} -> Retorna lista de cliente pelo nome e email



POST

http://{baseurl}/customers -> Inclusão de novo cliente.

Body:
	{
		"name": "nome cliente",
		"email": "email@gmail.com"
	}


PUT

http://{baseurl}/customers/{cliente id} -> Atualização de cliente cadastrado

Body:
	{
		"name": "nome cliente",
		"email": "email@gmail.com"
	}
	

DELETE

http://{baseurl}/customers/{cliente id}  -> Exclui Cliente cadastrado



#Produtos Favoritos


POST

http://{baseurl}/customerproduct - > Inclui Produto favorito ao cliente

Body:
	{
		"customerId": 1,
		"productId": "2bdaf48a-ce48-b798-9fbf-87c97f138a29"
	}
	

PUT

Analisado o requisito e não há necessidade de ter essa API.


DELETE

http://{baseurl}/customerproduct/{id} -> Exclui produto favorito de um cliente.	


GET

http://{baseurl}/customerproduct/{cliente id} -> Retorna lista de produtos favoritos do cliente.

