package br.com.jdouglas.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jdouglas.api.model.CustomerProduct;

public interface CustomerProductRepository extends JpaRepository<CustomerProduct, Long>  {
	
	List<CustomerProduct> findByCustomerId(Long customerId);
	
	List<CustomerProduct> findByCustomerIdAndProductId(Long customerId, String productId);
}
