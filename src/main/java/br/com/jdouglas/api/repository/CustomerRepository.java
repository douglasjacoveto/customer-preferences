package br.com.jdouglas.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jdouglas.api.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>  {
	
	List<Customer> findByName(String name);
	
	List<Customer> findByEmail(String email);
	
	List<Customer> findByNameAndEmail(String name, String email);

}
