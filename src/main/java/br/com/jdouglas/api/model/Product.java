package br.com.jdouglas.api.model;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {
	
	private UUID id;
	
	private String title;
	
	private String brand;
	
	private Double price;
	
	private String image;

}
