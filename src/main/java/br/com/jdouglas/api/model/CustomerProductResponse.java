package br.com.jdouglas.api.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerProductResponse {
	 private Long customerId;
	 
	 private String name;
	 
	 private String email;
	 
	 private List<Product> products;

}
