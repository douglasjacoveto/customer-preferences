package br.com.jdouglas.api.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.jdouglas.api.model.Product;

@Service
public class ProductService {
	static RestTemplate restTemplate = new RestTemplate();
	
	static String URL = "http://challenge-api.luizalabs.com/api/product/";
	
	public Boolean verifyProductExists(String productId) {
		Boolean isOk = true;
		return verifyProductService(productId);
	}
	
	public Product returnProduct(String productId) {
		ResponseEntity<Product> responseEntity =
			    restTemplate.getForEntity(URL+productId, Product.class);
		
		return responseEntity.getBody();
	}
	
	private static Boolean verifyProductService(String product) {
		
		
		try {
			System.out.println("Chamando Product Service");
			ResponseEntity<Product> responseEntity =
				    restTemplate.getForEntity(URL+product, Product.class);
//				System.out.println(responseEntity.getStatusCode());
//				System.out.println(responseEntity.getBody().getTitle());
				System.out.println("Produto " + responseEntity.getBody().getTitle() + " Cadastrado");
				return true;
			} catch(Exception e) {
				System.out.println("Produto " + product +" não Cadastrado");
				return false;
			}
	}
	

}
