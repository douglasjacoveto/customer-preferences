package br.com.jdouglas.api.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.jdouglas.api.message.MessageResponse;
import br.com.jdouglas.api.model.Customer;
import br.com.jdouglas.api.model.CustomerProduct;
import br.com.jdouglas.api.repository.CustomerProductRepository;
import br.com.jdouglas.api.repository.CustomerRepository;
import br.com.jdouglas.api.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "Customers")
@RestController
@RequestMapping("/customers")
public class CustomerController {
	
	@Autowired
    private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerProductRepository customerProductRepository;
	
	@Autowired
	private ProductService productService;
	
	@ApiOperation(value = "List customers (all, by name or by email)")
    @GetMapping
    public List<Customer> list(String name, String email) {

        if (name == null && email == null) {
            return customerRepository.findAll();
        }

        if (email==null) {
            return customerRepository.findByName(name);
        }

        if (name == null) {
            return customerRepository.findByEmail(email);
        }
        
        return customerRepository.findByNameAndEmail(name, email);

    }
	
	@ApiOperation(value = "Get a customer by ID")
    @GetMapping("/{id}")
    public ResponseEntity<Customer> getById(@PathVariable Long id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (customer.isPresent()) {
            return ResponseEntity.ok(customer.get());
        }

        return ResponseEntity.notFound().build();
    }
	
	@ApiOperation(value = "Insert a new customer")
    @PostMapping
    @Transactional
    public ResponseEntity<String> insert(@RequestBody @Valid Customer customer, UriComponentsBuilder uriComponentsBuilder) {	
		if(!verifyEmailDuplicate(customer)) {
			customerRepository.save(customer);
			URI uri = uriComponentsBuilder.path("/customers/{id}").buildAndExpand(customer.getCustomerId()).toUri();
        	return ResponseEntity.created(uri).body(MessageResponse.getMessageJson("Cliente Cadastrado: " + customer.getCustomerId()));
		 } else {
			 return ResponseEntity.badRequest().body(MessageResponse.getMessageJson("Email já cadastrado"));
		 }
    }
	
	@ApiOperation(value = "Update a customer by ID")
    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<String> update(@PathVariable Long id, @RequestBody @Valid Customer customer, UriComponentsBuilder uriComponentsBuilder) {
        if (customerRepository.existsById(id)) {
            customer.setCustomerId(id);
            if(!verifyEmailDuplicate(customer)) {
            	customerRepository.save(customer);
            	URI uri = uriComponentsBuilder.path("/customers/{id}").buildAndExpand(customer.getCustomerId()).toUri();
            	return ResponseEntity.created(uri).body(MessageResponse.getMessageJson("Cliente Alterado: " + customer.getCustomerId()));
    		
            	//return ResponseEntity.ok(customer.toString()); 
            } else {
            	return ResponseEntity.badRequest().body(MessageResponse.getMessageJson("Email já cadastrado"));
            }
            
        }

        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Delete a customer by ID")
    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> delete(@PathVariable Long id) {

        if (customerRepository.existsById(id)) {
        	
        	List<CustomerProduct> listCostumerProduct = customerProductRepository.findByCustomerId(id);
        	
        	for (CustomerProduct customerProduct : listCostumerProduct) {
        		customerProductRepository.deleteById(customerProduct.getId()); 		
        	}
        	
            customerRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
    
    private Boolean verifyEmailDuplicate(Customer parmCustomer) {
    	System.out.println(parmCustomer.toString());
    	List<Customer> listCustomer = customerRepository.findByEmail(parmCustomer.getEmail());
    	for (Customer customer : listCustomer) {
    		if(parmCustomer.getCustomerId()!=customer.getCustomerId()) {
    			return true;
    		}	
		}
   
		return false;
    	
    }
}
