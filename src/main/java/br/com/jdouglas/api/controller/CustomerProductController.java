package br.com.jdouglas.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.jdouglas.api.message.MessageResponse;
import br.com.jdouglas.api.model.CustomerProduct;
import br.com.jdouglas.api.model.Product;
import br.com.jdouglas.api.repository.CustomerProductRepository;
import br.com.jdouglas.api.repository.CustomerRepository;
import br.com.jdouglas.api.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "CustomersProduct")
@RestController
@RequestMapping("/customerproduct")
public class CustomerProductController {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerProductRepository customerProductRepository;
	@Autowired
	private ProductService productService;
	
	
	@ApiOperation(value = "List by customers")
	@GetMapping
	public List<CustomerProduct> list() {

		return customerProductRepository.findAll();

	}

	@ApiOperation(value = "List by customers")
	@GetMapping("/{customerId}")
	public List<Product> list(@PathVariable Long customerId) {
		
		List<CustomerProduct> listCostumerProduct = customerProductRepository.findByCustomerId(customerId);
		List<Product> favoriteProducts = new ArrayList<Product>();

		for (CustomerProduct customerProduct : listCostumerProduct) {
			System.out.println(customerProduct.getProductId());
			
			Product product = productService.returnProduct(customerProduct.getProductId());
			favoriteProducts.add(product);
		}
		
		return favoriteProducts;

	}

	@ApiOperation(value = "Insert a product to favorite list")
	@PostMapping
	@Transactional
	public ResponseEntity<String> insert(@RequestBody @Valid CustomerProduct customerProduct,
			UriComponentsBuilder uriComponentsBuilder) {
		String message = valideInsertFavoriteProduct(customerProduct);

		if (message.isEmpty()) {
			customerProductRepository.save(customerProduct);
			return ResponseEntity.ok().body(MessageResponse.getMessageJson("Produto Adicionado aos Favoritos: " + customerProduct.getProductId()));
		} else {
			return ResponseEntity.badRequest().body(MessageResponse.getMessageJson(message));
		}
	}

	private String valideInsertFavoriteProduct(CustomerProduct customerProduct) {
		String message = "";
		if (!productService.verifyProductExists(customerProduct.getProductId())) {
			message = "Produto " + customerProduct.getProductId() + " não cadastrado.";
		}

		if (!customerRepository.existsById(customerProduct.getCustomerId())) {
			message = "Cliente " + customerProduct.getCustomerId() + " não cadastrado.";
		}
		
		if(!customerProductRepository.findByCustomerIdAndProductId(customerProduct.getCustomerId(), customerProduct.getProductId()).isEmpty()) {
			message = "Cliente " + customerProduct.getCustomerId() + " já possui o produto " + customerProduct.getProductId() +" cadastrado";
		}
		
		
		return message;
	}

	@ApiOperation(value = "Delete a favorite product customer by ID")
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> delete(@PathVariable Long id) {

		if (customerProductRepository.existsById(id)) {
			customerProductRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();
	}

}
