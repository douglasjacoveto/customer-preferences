package br.com.jdouglas.api;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import br.com.jdouglas.api.model.Product;


public class TestConsumeApi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url = "http://challenge-api.luizalabs.com/api/product/77be5ad3-fa87-d8a0-9433-5dbcc3152fac";
		
		RestTemplate restTemplate = new RestTemplate();
//		try {
//			Product product = restTemplate.getForObject(url, Product.class);
//			System.out.println(product.getTitle() + " "+product.getPrice());
//		} catch(Exception e) {
//			System.out.println("Produto não encontrado");
//		}
	
		try {
		ResponseEntity<Product> responseEntity =
			    restTemplate.getForEntity(url, Product.class);
			System.out.println(responseEntity.getStatusCode());
			System.out.println(responseEntity.getBody().getTitle());
		} catch(Exception e) {
			System.out.println("Produto não encontrado");
		}

	}

}
