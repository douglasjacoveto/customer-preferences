package br.com.jdouglas.api.message;

import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageResponse {
	
	private String message;
	
	public static  String getMessageJson(String message) {
		Gson gson = new Gson();
		
		MessageResponse messageResponse = new MessageResponse();
		messageResponse.setMessage(message);

		return gson.toJson(messageResponse);
		
	}

}

